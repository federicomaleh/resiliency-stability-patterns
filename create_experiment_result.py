from pydantic import BaseModel


class CreateExperimentResult(BaseModel):
    id: str