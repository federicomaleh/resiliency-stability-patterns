from typing import List

from pydantic import BaseModel

from group import Group


class Experiment(BaseModel):
    name: str
    groups: List[Group]
