from experiment import Experiment


class ExperimentRepo:
    def __init__(self):
        self.__db = self.get_database()

    def save(self, experiment: Experiment):
        return self.__db.experiments.insert_one(experiment.dict())

    def get_database(self):
        from pymongo import MongoClient
        import pymongo

        # Provide the mongodb atlas url to connect python to mongodb using pymongo
        CONNECTION_STRING = "mongodb://localhost:27017/resiliency_pattern?ssl=false"

        # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
        from pymongo import MongoClient

        # Create the database for our example (we will use the same database throughout the tutorial
        return MongoClient(CONNECTION_STRING)['resiliency_pattern']
