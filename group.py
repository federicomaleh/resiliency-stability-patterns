from pydantic import BaseModel


class Group(BaseModel):
    name: str
    type: str
    exposure: float
