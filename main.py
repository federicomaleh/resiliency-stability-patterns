from typing import Optional

import uvicorn
from fastapi import FastAPI

from item import Item
from experiment import Experiment
from experiment_repo import ExperimentRepo
from create_experiment_result import CreateExperimentResult

app = FastAPI()

experiment_repo = ExperimentRepo()


@app.get("/ping")
def ping():
    return "PONG"


@app.post("/v1/experiments")
def create_experiment(experiment: Experiment) -> CreateExperimentResult:
    result = experiment_repo.save(experiment)

    return CreateExperimentResult(id=str(result.inserted_id))


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


@app.put("/items/{item_id}")
def update_item(item_id: int, item: Item):
    return {"item_name": item.name, "item_id": item_id}


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, log_level="info")
